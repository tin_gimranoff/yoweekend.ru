<?php

use yii\db\Migration;

/**
 * Class m171206_164146_add_link_to_category
 */
class m171206_164146_add_link_to_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
            $this->addColumn('event_categories', 'alias', $this->string(255)->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('event_categories', 'alias');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_164146_add_link_to_category cannot be reverted.\n";

        return false;
    }
    */
}
