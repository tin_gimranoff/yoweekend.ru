<?php

use yii\db\Migration;

/**
 * Class m171213_084525_add_position_to_category
 */
class m171213_084525_add_position_to_category extends Migration
{

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('event_categories', 'position', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('event_categories', 'position');
    }
    
}
