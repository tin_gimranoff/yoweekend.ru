<?php

use yii\db\Migration;

/**
 * Class m171130_121529_create_event_categories
 */
class m171130_121529_create_event_categories extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('event_categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('event_categories');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171130_121529_create_event_categories cannot be reverted.\n";

        return false;
    }
    */
}
