<?php

use yii\db\Migration;

/**
 * Class m171130_112940_add_events_table
 */
class m171130_112940_add_events_table extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'image_original' => $this->string()->defaultValue(NULL),
            'image_preview' => $this->string()->defaultValue(NULL),
            'link' => $this->string(255)->defaultValue(NULL),
            'city_id' => $this->integer()->notNull(),
            'address' => $this->string(255)->defaultValue(NULL),
            'meeting_date' => $this->date()->notNull(),
            'time_begin' => $this->time()->notNull(),
            'price' => $this->integer()->defaultValue(0),
            'description' => $this->text()->defaultValue(NULL),
            'isDraft' => $this->boolean()->defaultValue(false),
            'isVkSend' => $this->boolean()->defaultValue(false),            
        ]);
    }

    public function down()
    {
        $this->dropTable('events');
    }
    
}
