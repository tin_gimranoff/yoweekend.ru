<?php

use yii\db\Migration;

/**
 * Class m171225_191933_add_no_use_time_in_event
 */
class m171225_191933_add_no_use_time_in_event extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('events', 'no_time', $this->boolean()->defaultValue(false) . ' AFTER `time_begin`');
    }

    public function down()
    {
        $this->dropColumn('events', 'no_time');
    }
    
}
