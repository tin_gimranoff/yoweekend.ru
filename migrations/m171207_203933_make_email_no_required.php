<?php

use yii\db\Migration;

/**
 * Class m171207_203933_make_email_no_required
 */
class m171207_203933_make_email_no_required extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171207_203933_make_email_no_required cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171207_203933_make_email_no_required cannot be reverted.\n";

        return false;
    }
    */
}
