<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city`.
 */
class m171118_120040_create_city_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alias' => $this->string()->notNull(),
            'hidden' => $this->boolean()->defaultValue(false),
        ]); 
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('city');
    }
}
