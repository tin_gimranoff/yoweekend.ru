<?php

use yii\db\Migration;

/**
 * Class m171201_120426_add_fields_to_event
 */
class m171201_120426_add_fields_to_event extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('events', 'user_id', $this->integer()->notNull());
        $this->addColumn('events', 'create_date', $this->dateTime());
        $this->addColumn('events', 'update_date', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('events', 'user_id');
        $this->dropColumn('events', 'create_date');
        $this->dropColumn('events', 'update_date');
    }
    
}
