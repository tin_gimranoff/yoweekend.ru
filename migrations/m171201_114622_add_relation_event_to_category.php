<?php

use yii\db\Migration;

/**
 * Class m171201_114622_add_relation_event_to_category
 */
class m171201_114622_add_relation_event_to_category extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('rel_event_to_category', [
           'id' => $this->primaryKey(),
           'event_id' => $this->integer()->notNull(),
           'event_category_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('rel_event_to_category');
    }
}
