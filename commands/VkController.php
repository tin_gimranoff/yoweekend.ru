<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\components\Vkontakte;
use app\models\Event;
use app\models\EventCategory;
use yii\helpers\ArrayHelper;

class VkController extends Controller {

    private $hash_tags_const = ['обнинск', 'событияобнинск', 'obnx', 'yoweekend'];

    public function actionSendPost() {
        $date = new \DateTime();
        $date->modify('+1 day');
        $date = $date->format('Y-m-d');
        $events = Event::find()->where(['isVkSend' => 0, 'meeting_date' => $date])->orderBy('create_date ASC')->limit(5)->all();
        if (count($events) > 0)
            $vkAPI = new Vkontakte(['access_token' => Yii::$app->params['access_token']]);
        foreach ($events as $e) {
            $image = dirname(__FILE__) . '/../web' . str_replace('\\', '/', $e->image_original);
            $categories_ids = $e->categoriesIds;
            $categories = ArrayHelper::map(EventCategory::find()->all(), 'id', 'name');
            $hash_tags = [];
            foreach ($categories_ids as $c_id) {
                $hash_tags[] = str_replace(' ', '_', $categories[$c_id->event_category_id]);
            }
            $hash_tags = array_merge($hash_tags, $this->hash_tags_const);
            if($e->no_time == 1)
                    $time = '';
            else
                $time = " в " . Yii::$app->formatter->asTime($e->time_begin, 'HH:mm');
            $message = Yii::$app->formatter->asDate($e->meeting_date, 'd MMMM yyyy') . $time . " состоится \n";
            $message .= $e->name . "\n\n";
            $message .= mb_strimwidth($e->description, 0, 300, '...') . "\n\n";
            if (!empty($e->link))
                $message .= "Подробнее по ссылке: " . $e->link . "\n\n";
            else {
                $link = "http://yoweekend.ru/".$e->city->alias."/event_".$e->id;
                $message .= "Подробнее по ссылке: " . $link . "\n\n";
            }
                
            $message .= "______________\nБольше мероприятий и событий на сайте http://yoweekend.ru! Так же добавляй свои мероприятия на этот сайт или в этой группе, просто написав сообщение.";

            if ($vkAPI->postToPublic(Yii::$app->params['group_id'], $message, $image, $hash_tags)) {
                $e->isVkSend = 1;   
            } else {
                $e->isVkSend = -1;
            }
            $e->save(false);
            sleep(20);
        }

    }

}
