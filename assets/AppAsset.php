<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'libs/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'libs/adminlte/bower_components/font-awesome/css/font-awesome.min.css',
        'libs/adminlte/bower_components/Ionicons/css/ionicons.min.css',
        'libs/adminlte/dist/css/AdminLTE.css?v=0001',
        'libs/adminlte/dist/css/skins/_all-skins.min.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
        'libs/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
        'libs/adminlte/plugins/timepicker/bootstrap-timepicker.min.css',
        'libs/adminlte/plugins/iCheck/all.css',
        'libs/adminlte/bower_components/select2/dist/css/select2.min.css',
        'libs/img-area-select/imgareaselect-deprecated.css',
        'libs/shadowbox/shadowbox.css',
        'css/style.css?v=0038'
    ];
    public $js = [
        'libs/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'libs/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'libs/adminlte/plugins/timepicker/bootstrap-timepicker.min.js',
        'libs/adminlte/plugins/iCheck/icheck.min.js',
        'libs/adminlte/bower_components/select2/dist/js/select2.full.min.js',
        'libs/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'libs/img-area-select/jquery.imgareaselect.js',
        'libs/adminlte/dist/js/adminlte.min.js',
        'libs/context-menu/BootstrapMenu.min.js',
        'libs/shadowbox/shadowbox.js',
        'https://api-maps.yandex.ru/2.1.42/?lang=ru_RU',
        'libs/jquery-cookie/jquery.cookie.js',
        'js/script.js?v=0149'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
