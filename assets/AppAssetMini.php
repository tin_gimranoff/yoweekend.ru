<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetMini extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'libs/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'libs/adminlte/bower_components/font-awesome/css/font-awesome.min.css',
        'libs/adminlte/dist/css/AdminLTE.css',
        'libs/font-awesome/css/font-awesome.min.css',
        'libs/adminlte/plugins/iCheck/square/blue.css',
        'css/style.css?v=0001'
    ];
    public $js = [
        'libs/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'libs/adminlte/plugins/iCheck/icheck.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
