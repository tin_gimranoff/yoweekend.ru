<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
    <? if(!empty(Yii::$app->session->getFlash('success'))): ?>
            <div class="callout callout-info">
                <h4><i class="fa fa-info"></i> Успех:</h4>
                <?= Yii::$app->session->getFlash('success')?>
            </div>
            <? Yii::$app->session->removeFlash('success')?>
    <? endif; ?>
    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'retype_password')->passwordInput() ?>
        <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-left: 0; ">
            <button type="submit" class="btn btn-block btn-primary btn-flat">Сохранить</button>
        </div>
</div>
<?php ActiveForm::end(); ?>
