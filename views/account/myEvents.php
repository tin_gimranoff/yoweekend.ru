<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 add-event-not-register">
    <button type="button" class="btn btn-block btn-success btn-sm"
            onclick="window.location.href = '/event/add'">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Добавить мероприятие
    </button>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 isDraftLink">
    <label class='draft-checkbox-label'>
        <input type="checkbox" class="minimal" <?= ($is_draft == 'drafts') ? 'checked' : ''?> > <span>Показать только черновики</span>
    </label>
</div>    
<div class="clear" style="clear: both"></div>
<?= Yii::$app->view->render('../site/events', ['events' => $events]) ?>

