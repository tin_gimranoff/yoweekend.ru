<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 full-info-modal">
    <img class="img-responsive" src="<?= str_replace('\\', '/', $data->image_preview) ?>">
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 full-info-modal">
    <div class="caption full-info-modal-caption">
        <span class="icon-panel">
            <a class="btn btn-default" href="https://vk.com/share.php?url=http://<?= $_SERVER['SERVER_NAME'] ?>/<?= $data->city->alias?>/event_<?= $data->id ?>&title=<?= htmlspecialchars($data->name, ENT_QUOTES) ?>&image=<?= 'http://' . $_SERVER['SERVER_NAME'] . str_replace('\\', '/', $data->image_original) ?>&noparse=true" target="_blank">
                Рассказать друзьям <i class="fa fa-share-alt"></i>
            </a>
            <!--<a href="#" class="share-icon"><i class="fa fa-share-alt"></i></a>-->
        </span>
        <span class="link"><a href="<?= $data->link ?>" target="_blank"><?= mb_strimwidth($data->link, 0, 30, '...') ?></a></span>
        <? if($data->no_time == 0): ?>
            <span class="datetime">
                <strong class="strong-underline">Время:</strong> 
                <?= Yii::$app->formatter->asTime($data->time_begin) ?>
            </span>
        <? else: ?>
            <span class="datetime">
                <strong class="strong-underline">Время:</strong> 
                Не указано
            </span>
        <? endif; ?>
        <?php
        if (empty($data->price) || $data->price == 0)
            $price = 'Бесплатно';
        else
            $price = $data->price . '<i class="fa fa-rub" aria-hidden="true"></i>';
        ?>
        <span class="money"><strong class="strong-underline">Вход:</strong> <?= $price ?></span>
    </div>
</div>
<div class="clear"></div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 full-info-modal-intro">
    <span class="intro"><?= $text ?></span>
</div>
<div class="clear"></div>
