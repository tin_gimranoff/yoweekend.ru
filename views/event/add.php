<?php
use dosamigos\fileupload\FileUpload;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm
?>
<?php
$flash_title = '';
$flash_message = '';
$flash_class = '';
if(!empty(Yii::$app->session->getFlash('error'))) {
    $flash_title = 'Ошибка';
    $flash_message = Yii::$app->session->getFlash('error');
    $flash_class = 'callout-warning';
    Yii::$app->session->removeFlash('error');
} 
if(!empty(Yii::$app->session->getFlash('success'))) {
    $flash_title = 'Успех!';
    $flash_message = Yii::$app->session->getFlash('success');
    $flash_class = 'callout-success';
    Yii::$app->session->removeFlash('success');
}

?>
<? if(!empty($flash_message)): ?>
    <div class="callout <?= $flash_class ?>" style="">
            <h4><i class="fa fa-info"></i> <?= $flash_title?></h4>
            <?= $flash_message ?>
    </div>
<? endif; ?>
<?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
        <label>Афиша</label>
        <div class="input-group">
            <!-- <button class="btn btn-primary btn-sm">Загрузить изображение</button> -->
            <?= FileUpload::widget([
                'model' => $model,
                'attribute' => 'image',
                'url' => ['media/upload'], // your url, this is just for demo purposes,
                'options' => [
                    'accept' => 'image/*',
                ],                
                'clientOptions' => [
                    'maxFileSize' => 2000000,
                    'dataType' => 'json',
                ],
                // Also, you can specify jQuery-File-Upload events
                // see: https://github.com/blueimp/jQuery-File-Upload/wiki/Options#processing-callback-options
                'clientEvents' => [
                    'fileuploaddone' => 'function(e, data) {
                                if(typeof(data.result.status) != undefined && data.result.status == false) {
                                    show_info_modal("Ошибка!", data.result.textError);
                                } else {
                                    $(".field-event-image_original").removeClass("has-error");
                                    $(".field-event-image_original p").empty();
                                    $("#event-image_original").val(data.result.files[0].url);
                                    $("#event-image_preview").val(data.result.files[0].thumbnailUrl);
                                    $(".object-image").imgAreaSelect( {remove: true} );
                                    $(".object-image").attr("src", data.result.files[0].thumbnailUrl).imgAreaSelect({                                       
                                        handles: true,
                                        onSelectEnd: setImgCoordinates,
                                        onInit: setImgCoordinates,
                                        minWidth: 299,
                                        minHeight: 299,
                                        maxWidth: 300,
                                        maxHeight: 300,
                                        show: true,
                                        imageHeight: data.result.files[0].height,
                                        imageWidth: data.result.files[0].width,
                                        x1: 0,
                                        x2: 298,
                                        y1: 0,
                                        y2: 298,
                                    });
                                }
                            }',
                    'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                                show_info_modal("Ошибка!", "В процессе загрузки файла произошла ошибка");
                            }',
                ],
            ]); ?>
            <div class="img_preview">
                <? $img_size = $model->getRealImgSize($model->image_preview);?>
                <img class="img-responsive object-image" src="<?= $model->image_preview ?>" data-real-width="<?= $img_size['width']?>" data-real-height="<?= $img_size['height']?>">
            </div>
        </div>
    </div>
    <?= $form->field($model, 'image_original')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'image_preview')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'x1')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'x2')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'y1')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'y2')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'name')->textInput() ?>
    <?= $form->field($model, 'category_ids', [
            'template' => '{label}{input}{error}{hint}<a href="#" data-toggle="modal" data-target="#add-new-category">Если вы не нашли свою категорию, нажмите на эту ссылку</a>'
        ])->dropDownList($category_arr, ['class' => 'form-control select2', 'multiple' => true, 'aria-hidden' => true])?>
    <?= $form->field($model, 'link')->textInput() ?>
    <?= $form->field($model, 'city_id')->dropDownList($city_arr, ['class' => 'form-control select2', 'multiple' => false, 'aria-hidden' => true])?>
    <?= $form->field($model, 'address')->textInput() ?>
    <?= $form->field($model, 'meeting_date', [
        'template' => '{label}<div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div>{input}</div>{error}{hint}'
    ])->textInput(['class' => 'form-control datepicker-modal', 'data-inputmask' => "'alias': 'dd/mm/yyyy'", 'data-mask' => "", "readonly" => true]) ?>
    <div class="bootstrap-timepicker">
        <?= $form->field($model, 'time_begin', [
            'template' => '{label}<div class="input-group"><div class="input-group-addon"><i class="fa fa-clock-o"></i></div>{input}</div>{error}{hint}'
        ])->textInput(['class' => 'form-control input-small timepicker', "readonly" => true]) ?>
        <?= $form->field($model, 'no_time')->checkbox(['class' => 'minimal', 'value' => '1'])->label('Не показывать время на сайте (не желательно)', ['class'=> 'draft-checkbox-label']); ?>
    </div>
    <?= $form->field($model, 'price', [
            'template' => '{label}<div class="input-group"><div class="input-group-addon"><i class="fa fa-rub"></i></div>{input}<span class="input-group-addon">.00</span></div>{error}{hint}'
        ])->textInput() ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
    <?= $form->field($model, 'isDraft')->checkbox(['class' => 'minimal', 'value' => '1'])->label('Черновик', ['class'=> 'draft-checkbox-label']); ?>
    <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-left: 0; ">
        <button type="submit" class="btn btn-block btn-primary btn-flat">Сохранить</button>
    </div>
<?php ActiveForm::end(); ?>
<div class="modal fade in" id="add-new-category">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="callout" style="display: none;">
                        <h4><i class="fa fa-info"></i></h4>
                        <span>dfgdfg</span>
                </div>
                <form>
                    <div class="form-group category-name required">
                        <label class="control-label" for="category-name">Введите название предлагаемой категории</label>
                        <input type="text" class="form-control">
                        <p class="help-block help-block-error"></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" id="send-new-category">Отправить</button>
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>