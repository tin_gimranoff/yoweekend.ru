<?php

use app\assets\AppAsset;
use yii\helpers\Html;
use app\models\City;
use app\widgets\FilterWidget;
use yii\helpers\Url;

$this->registerJsFile('/libs/adminlte/bower_components/jquery/dist/jquery.min.js');
AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/favicon.png'])]);
if(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') {
    $this->registerJsFile('/js/smallpanel.js?v=0009');
    $this->registerJsFile('/js/moreEventContent.js?v=0014');
}
if(Yii::$app->controller->id == 'account' && Yii::$app->controller->action->id == 'my-events') {
    $this->registerJsFile('/js/smallpanel.js?v=0009');
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue layout-top-nav sidebar-collapse">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <!-- Main Header -->
            <!-- HEADER -->
            <header class="main-header">
                <!-- Logo -->
                <a href="/" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>YO</b>weekend</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <!-- <object data="/img/logo.svg" type="image/svg+xml"></object> -->
                        <img src="/img/logo.png" />
                    </span>
                </a>
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="container-fluid">
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">                                    
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?= City::detectSavedCity() ?><span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <? foreach(City::getCitiesArr() as $city): ?>                                            
                                            <li><a href="/<?= $city['alias'] ?>"><?= $city['name']?></a></li>
                                        <? endforeach; ?>                                                                                
                                    </ul>
                                </li>
                                <?php /* Расскомментировать когда будет внедряться функционал мест 
                                <li class="dropdown point-type-menu mode-dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <?php
                                            if(Yii::$app->getRequest()->getCookies()['mode'] == '1')
                                                echo 'Мероприятия';
                                            if(Yii::$app->getRequest()->getCookies()['mode'] == '2')
                                                echo 'Места';
                                        ?>
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu" >                                        
                                        <li><a href="#" data-value="1">Мероприятия</a></li>
                                        <li><a href="#" data-value="2">Места</a></li>
                                    </ul>
                                </li>
                                */ ?>
                                <li class="dropdown filter-menu">
                                    <!-- Menu toggle button -->
                                    <a href="#" data-toggle="modal" data-target="#modal-filter">
                                        <i class="fa fa-filter"></i>&nbsp;&nbsp;Фильтр
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="http://vk.com/yoweekend_obnx" target="_blank" class="header-vk-icon"><img src="/img/vk.png"></a></li>
                                <?php if(!Yii::$app->user->isGuest): ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Мой профиль <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="/account/change-password">Сменить пароль</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="/account/my-events">Мои мероприятия</a></li>                                            
                                            <li><a href="/event/add">Добавить мероприятие</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#" data-toggle="modal" data-target="#donate-modal">Поддержка проекта</a></li>
                                            <li><a href="/logout">Выйти</a></li>
                                        </ul>
                                    </li>
                                <? else: ?>
                                    <li>
                                        <a type="button" href="/login">Войти</a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                </nav>
            </header>
            <header class="main-header main-header-compact">
                <div class="container">
                    <a href="#" class="sidebar-toggle sidebar-toggle-compact" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#modal-filter" class="menu-element-compact">
                        <i class="fa fa-filter"></i>
                    </a>
                    <div class="date-label"></div>
                </div>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <!-- END_HEADER -->
            <!-- SIDEBAR -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu sidebar-category-links" data-widget="tree">
                        <li class="header">Мероприятия</li>
                        <!-- Optionally, you can add icons to the links -->
                        <li><a href="/<?= City::detectSavedCity(true)->alias ?>"><span>Все категории</span></a></li>
                        <? foreach(\app\models\EventCategory::getCategoryList() as $c): ?>
                            <li><a href="/<?= City::detectSavedCity(true)->alias ?>/<?= $c->alias ?>"><span><?= $c->name ?></span></a></li>
                        <? endforeach; ?>
                    </ul>
                    <?php echo app\widgets\filterWidget\FilterWidget::widget(['view' => 'sidebar_form']); ?>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- END_SIDEBAR -->
            <!-- CONTENT -->
            <div class="content-wrapper" style="min-height: 600px;">
                <div class="container content-container">
                    <?= $content ?>
                </div>
                <? if(Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index'): ?>
                    <div class="more-load-btn"><button class="btn btn-sm">Загрузить еще</button></div>
                <? endif; ?>
            </div>
            <!-- END_CONTENT -->
            <!-- FOOTER -->
            <footer class="main-footer">
                Если есть вопросы, то пишите на почту: <a href="mailto: support@yoweekend.ru">support@yoweekend.ru</a>
            </footer>
            <!-- END_FOOTER -->
        </div>
        <!-- MODALS -->
        <?php echo app\widgets\filterWidget\FilterWidget::widget([]); ?>
        <div class="modal fade in" id="info-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>  
        <div class="modal fade in" id="donate-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Сказать спасибо</h4>
                    </div>
                    <div class="modal-body">
                        Проект разрабатывается и поддерживается полностью на энтузиазме с целью сделать мир немножечко лучше. <br />
                        <strong>Если вам понравился проект, то вы можете сказать свое "спасибо".</strong>
                        <br /><br />
                        <div class="payment-small">
                            <iframe src="https://money.yandex.ru/quickpay/button-widget?targets=%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D1%83%20%D0%BD%D0%B0%20%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5&default-sum=50&button-text=11&any-card-payment-type=on&button-size=m&button-color=orange&successURL=http%3A%2F%2Fyoweekend.ru&quickpay=small&account=410013033407088&" width="184" height="36" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
                        </div>
                        <div class="payment-large">
                            <iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC%20%D0%BD%D0%B0%20%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D1%8C%D0%B5&targets-hint=&default-sum=10&button-text=11&payment-type-choice=on&hint=&successURL=http%3A%2F%2Fyoweekend.ru&quickpay=shop&account=410013033407088" width="450" height="213" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- END_MODALS -->
        <div class="custom-load-layout">
            <img src="/img/ajax-loader.svg" />
        </div>
        <a href="#" class="scrollup">Наверх</a>
        <?php if($_SERVER['SERVER_NAME'] == 'yoweekend.ru'): ?>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter47022225 = new Ya.Metrika({
                            id:47022225,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/47022225" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <?php endif; ?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>