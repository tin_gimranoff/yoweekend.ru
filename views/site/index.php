<? if(Yii::$app->user->isGuest): ?>
    <? $onclick_title = 'Требуется авторизация'; ?>
    <? $onclick_message = "Для того что бы доавблять свои мероприятия вам необходимо <a href=\'/login\'>войти</a> или <a href=\'/register\'>зарегистрироваться</a>.";?>
<? else: ?>
    <? //$onclick_title = 'Что вы хотите добавить?'; ?>
    <? //$onclick_message = "Выберите действие:<br /><a href=\'/event/add\'>добавить мероприятие</a> или <a href=\'/point/add\'>добавить место</a>?";?>
<? endif; ?>
<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12 add-event-not-register">
    <? if(isset($onclick_message) && !empty($onclick_message)): ?>
    <button type="button" class="btn btn-block btn-success btn-sm"
            onclick="show_info_modal('<?= $onclick_title ?>', '<?= $onclick_message ?>')">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Добавить мероприятие
    </button>
    <? else: ?>
    <button type="button" class="btn btn-block btn-success btn-sm"
            onclick="window.location.href='/event/add'">
        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Добавить мероприятие
    </button>
    <? endif; ?>
</div>
<div class="clear" style="clear: both"></div>
<?= Yii::$app->view->render('_activeFilter', ['filter' => $filter]) ?>
<input type="hidden" name="filter_data" value='<?= $filter_query ?>'>
<?= Yii::$app->view->render('events', ['events' => $events, 'city' => $city]) ?>
<?php if($event_info != null): ?>
        <div class="modal fade in" id="event_modal_page">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="/<?= $city->alias ?>"><button type="button" class="close" aria-label="Close">
                                <span aria-hidden="true">×</span></button></a>
                        <h4 class="modal-title"><?= $event_info->name ?></h4>
                    </div>
                    <div class="modal-body">
                        <?= Yii::$app->view->renderFile(dirname(__FILE__) . '/../ajax/fullInfoModal.php', [
                            'data' => $event_info,
                            'text' => $event_info_text,
                        ])?>
                    </div>
                    <div class="modal-footer">
                        <a href="/<?= $city->alias ?>"><button type="button" class="btn btn-default pull-left">Закрыть</button></a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div> 
<script>
    window.onload = function() {
        $("#event_modal_page").modal('show');
        $("#event_modal_page").on('hidden.bs.modal', function(){
           window.location.href = '/<?= $city->alias ?>'; 
        });
    }
</script>
<?php endif; ?>
