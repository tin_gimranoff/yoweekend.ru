<?
    if($city == NULL)
        $city = $e->city->name;
?>
<div class="card col-lg-3 col-md-4 col-sm-4 col-xs-6">
    <div class="thumbnail">
        <div class="image-preview">
            <img src="<?= $e->image_preview ?>" data-full-img="<?= $e->image_original ?>" data-address="<?= $city?>, <?= $e->address ?>" data-id="<?= $e->id ?>">
            <? if(!empty($e->address)): ?>
            <span class="address"><a href="#" class="openMap" data-address="<?= $city?>, <?= $e->address ?>" onclick="openMap($(this).attr('data-address'));return false;"><i class="fa fa-map-marker address-marker"></i><span class="address-text"><?= $e->address ?></span></a></span>
            <? endif; ?>
        </div>
        <div class="caption">
            <span class="icon-panel">
                <a class="share-icon" href="https://vk.com/share.php?url=http://<?= $_SERVER['SERVER_NAME'] ?>/<?= $e->city->alias?>/event_<?= $e->id ?>&title=<?= htmlspecialchars($e->name, ENT_QUOTES) ?>&image=<?= 'http://'.$_SERVER['SERVER_NAME'].str_replace('\\', '/', $e->image_original) ?>&noparse=true" target="_blank">
                    <i class="fa fa-share-alt"></i>
                </a>
                <!--<a href="#" class="share-icon"><i class="fa fa-share-alt"></i></a>-->
            </span>
            <span class="name"><a href="/<?= $e->city->alias?>/event_<?= $e->id ?>" class="open-event-window" data-id="<?= $e->id ?>" onclick="showFullInfo($(this).attr('data-id'), '/ajax/get-full-info-modal');return false;"><?= mb_strimwidth($e->name, 0, 60, '...') ?></a></span>
            <span class="link"><a href="<?= $e->link ?>" target="_blank"><?= mb_strimwidth($e->link, 0, 30, '...') ?></a></span>
            <? if($e->no_time == 0):?>
                <span class="datetime">
                    <strong class="strong-underline">Время:</strong> 
                    <?= Yii::$app->formatter->asTime($e->time_begin) ?>
                </span>
            <? else: ?>
                <span class="datetime">
                    <strong class="strong-underline">Время:</strong> 
                    Не указано
                </span>
            <? endif; ?>
            <?php
            if (empty($e->price) || $e->price == 0)
                $price = 'Бесплатно';
            else
                $price = $e->price . '<i class="fa fa-rub" aria-hidden="true"></i>';
            ?>
            <span class="money"><strong class="strong-underline">Вход:</strong> <?= $price ?></span>
            <span class="intro"><a href="#" class="showFullInfo" data-id="<?= $e->id ?>" onclick="showFullInfo($(this).attr('data-id'), '/ajax/get-full-info');return false;"><?= mb_strimwidth($e->description, 0, 100, '...') ?></a></span>
        </div>
    </div>
</div>

