<? if(!isset($date)) $date = NULL; ?>
<? foreach($events as $e): ?>
    <? if($date == NULL || ($date != $e->meeting_date && $date != Yii::$app->formatter->asDate($e->meeting_date, 'd MMMM yyyy'))): ?>
        <? $date = $e->meeting_date ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 date-label-area">
            <div class="date-label"><?= Yii::$app->formatter->asDate($date, 'd MMMM yyyy') ?></div>
        </div>
    <? endif; ?>
    <?= Yii::$app->view->render('_event', ['e' => $e, 'city' => (isset($city)) ? $city->name : NULL]) ?>
<? endforeach; ?>
