<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
?>
<div class="login-box">
    <div class="register-logo">
        <a href="/"><img src="/img/logo.png" /></a>
    </div>
    <div class="login-box-body">
        <? if(!empty(Yii::$app->session->getFlash('success'))): ?>
            <div class="callout callout-info">
                <h4><i class="fa fa-info"></i> Успех:</h4>
                <?= Yii::$app->session->getFlash('success')?>
            </div>
        <? endif; ?>
        <? if(!empty(Yii::$app->session->getFlash('error'))): ?>
            <div class="callout callout-info">
                <h4><i class="fa fa-info"></i> Успех:</h4>
                <?= Yii::$app->session->getFlash('error')?>
            </div>
        <? endif; ?>
        <?php $form = ActiveForm::begin(['id' => 'login']); ?>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'username', [
                'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-user form-control-feedback"></span>'
            ])->textInput(['autofocus' => false, 'placeholder' => 'Логин']) ?>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password', [
                'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-lock form-control-feedback"></span>'
            ])->passwordInput(['autofocus' => false, 'placeholder' => 'Пароль']) ?>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                        <?= $form->field($model, 'rememberMe', [
                            'template' => '{input}'
                        ])->checkbox() ?>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>

        <div class="social-auth-links text-center">
            <a href="/site/auth?authclient=vkontakte" class="btn btn-block btn-social btn-vk btn-flat"><i class="fa fa-vk"></i> Войти с помощью VK</a>
        </div>
        <!-- /.social-auth-links -->
        <a href="/">Вернуться на сайт</a><br>
        <a href="/request-password-reset">Забыли пароль?</a><br>
        <a href="/register" class="text-center">Зарегистрироваться</a>

    </div>
    <!-- /.login-box-body -->
</div>