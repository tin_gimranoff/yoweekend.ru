<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="login-box">
    <div class="register-logo">
        <a href="/"><img src='/img/logo.png'></a>
    </div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'username', [
                        'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-user form-control-feedback"></span>'
                ])->textInput(['placeholder' => 'Логин']) ?>
            </div>
            <div class="form-group has-feedback">
                <div class="form-group has-feedback">
                    <?= $form->field($model, 'email', [
                        'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>'
                    ])->textInput(['placeholder' => 'Email']) ?>
                </div>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'password', [
                    'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-lock form-control-feedback"></span>'
                ])->passwordInput(['placeholder' => 'Пароль']) ?>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'retype_password', [
                    'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-log-in form-control-feedback"></span>'
                ])->passwordInput(['placeholder' => 'Пароль еще раз']) ?>
            </div>
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-9 pull-right">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Зарегистрироваться</button>
                </div>
                <!-- /.col -->
            </div>
        <?php ActiveForm::end(); ?>

        <div class="social-auth-links text-center">
            <a href="/site/auth?authclient=vkontakte" class="btn btn-block btn-social btn-vk btn-flat"><i class="fa fa-vk"></i> Войти с помощью VK</a>
        </div>
        <!-- /.social-auth-links -->
        <a href="/">Вернуться на сайт</a><br>
        <a href="/login" class="text-center">Я уже зарегистрирован</a>

    </div>
    <!-- /.login-box-body -->
</div>