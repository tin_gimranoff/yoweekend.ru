<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <ul class="breadcrumb">
      <li>Выбранные категории: <span class="filter-active-params">
                <?= implode(', ',$filter->getStringCategories())?>
      </span></li>
      <? if(!empty($filter->meeting_date)): ?>
        <li>Начиная с: <span class="filter-active-params"><?= Yii::$app->formatter->asDate($filter->meeting_date, 'd MMMM yyyy') ?></span></li>
      <? else: ?>
        <li>Начиная с: <span class="filter-active-params"><?= Yii::$app->formatter->asDate(date('Y-m-d'), 'd MMMM yyyy') ?></span></li>
      <? endif; ?>
      <? if(!empty($filter->time_begin)): ?>
        <? ($filter->time_begin == '00:00') ? $timebegin = 'Не важно' : $timebegin = $filter->time_begin ?>
        <li>Время начала с: <span class="filter-active-params"><?= $timebegin ?></span></li>
      <? endif; ?>
    </ul>
</div>
