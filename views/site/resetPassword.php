<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="login-box">
    <div class="register-logo">
        <a href="/"><img src='/img/logo.png'></a>
    </div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'password', [
                'template' => '{input}{error}{hint}<span class="glyphicon glyphicon-enviroment form-control-feedback"></span>'
            ])->passwordInput(['autofocus' => true, 'placeholder' => 'Пароль']) ?>
        </div>
        <div class="row">
            <!-- /.col -->
            <div class="col-xs-7 pull-right">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Сохранить</button>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.login-box-body -->
</div>