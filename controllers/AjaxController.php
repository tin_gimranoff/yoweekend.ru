<?php

/**
 * Created by PhpStorm.
 * User: tin
 * Date: 26.11.2017
 * Time: 21:55
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Filter;

class AjaxController extends Controller {

    public function actionGetFullInfo() {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            echo '';
            Yii::$app->end();
        }
        $data = \app\models\Event::find()->where(['id' => $_POST['id']])->one();
        if (!$data) {
            echo '';
            Yii::$app->end();
        }
        $text = preg_replace('/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/i', '<a href="$1" target="_blank">$1</a>', $data->description);
        $text = preg_replace('/<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"/i', '<a href="mailto:$1"', $text);
        echo json_encode([
            'name' => $data->name,
            'html' => nl2br($text),
        ]);
        Yii::$app->end();
    }
    
    public function actionGetFullInfoModal() {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            echo '';
            Yii::$app->end();
        }
        $data = \app\models\Event::find()->where(['id' => $_POST['id']])->one();
        if (!$data) {
            echo '';
            Yii::$app->end();
        }
        $text = preg_replace('/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/i', '<a href="$1" target="_blank">$1</a>', $data->description);
        $text = preg_replace('/<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"/i', '<a href="mailto:$1"', $text);
        $text = nl2br($text);
        echo json_encode([
            'name' => $data->name,
            'html' => $this->renderPartial('fullInfoModal', [
                        'data' => $data,
                        'text' => $text,
                    ])
        ]);
    }

    public function actionGetEventContent() {
        $filter = new Filter;
        $filter->attributes = json_decode($_POST['filterData'], true);
        $city = \app\models\City::detectSavedCity(true);
        $events = \app\models\Event::find()->joinWith('categoriesIds')
                ->where(['city_id' => $city->id])
                ->andWhere(['isDraft' => 0]);
        if (empty($filter->meeting_date) && (empty($filter->time_begin) || $filter->time_begin == '00:00')) {
	    $events = $events->andWhere(['OR', ['>=', 'CONCAT(meeting_date, " ",time_begin)', date('Y-m-d H:i:s')], ['AND', ['>=','meeting_date',date('Y-m-d')], 'no_time' => 1]]);
            //$events = $events->andWhere(['>=', 'CONCAT(meeting_date, " ",time_begin)', date('Y-m-d H:i:s')]);
        } elseif (empty($filter->meeting_date) && !empty($filter->time_begin) && $filter->time_begin != '00:00') {
            $events = $events->andWhere(['>=', 'meeting_date', date('Y-m-d')]);
            $events = $events->andWhere(['>=', 'time_begin', $filter->time_begin]);
        } elseif (!empty($filter->meeting_date)) {
            $events = $events->andWhere(['>=', 'meeting_date', $filter->meeting_date]);
            if (!empty($filter->time_begin))
                $events = $events->andWhere(['>=', 'time_begin', $filter->time_begin]);
        }
        if (!empty($filter->category))
            $events = $events->andWhere(['rel_event_to_category.event_category_id' => $filter->category]);
        $events = $events->orderBy('meeting_date, time_begin')
                ->limit(Yii::$app->params['limit_objects_on_page'])
                ->offset($_POST['startFrom'])
                ->groupBy('id')
                ->all();
        return $this->renderPartial('../site/events', [
                    'events' => $events,
                    'city' => $city,
                    'date' => Yii::$app->request->post('lastDate', NULL),
        ]);
    }
    
    public function actionSendNewCategory() {
        if(!Yii::$app->request->isAjax) {
            echo json_encode(['status' => false, 'textMessage' => 'В процессе произошла ошибка.']);
            Yii::$app->end();
        }
        $category_name = Yii::$app->request->post('category_name', NULL);
        if($category_name == NULL){
            echo json_encode(['status' => false, 'textMessage' => 'Название категории не может быть пустым.']);
            Yii::$app->end();
        }
        
        $subject = "Запрос на добавление категории";
        
        $send_msg = Yii::$app
            ->mailer
            ->compose(
                ['text' => 'sendNewCategory'],
                ['category_name' => $category_name, 'user_id' => Yii::$app->user->id]
            )
            ->setFrom([Yii::$app->params['noreplyEmail'] => 'no-reply@'.Yii::$app->name])
            ->setTo(Yii::$app->params['supportEmail'])
            ->setSubject($subject)
            ->send();
        if($send_msg) {
            echo json_encode(['status' => true, 'textMessage' => 'Ваша заявка отправлена, вы можете пока сохранить свое мероприятие, как черновик. После добавления новой категории мы с вами свяжемся.']);
            Yii::$app->end();
        } else {
            echo json_encode(['status' => false, 'textMessage' => 'В процессе отправки сообщения произошла ошибка. Повторите запрос позднее.']);
            Yii::$app->end();
        }
    }

}
