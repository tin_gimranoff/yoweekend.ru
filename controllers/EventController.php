<?php
/**
 * Created by PhpStorm.
 * User: tin
 * Date: 26.11.2017
 * Time: 21:14
 */

namespace app\controllers;

use app\models\Event;
use Yii;
use yii\web\Controller;
use app\models\EventCategory;
use app\models\City;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;
use app\models\RelEventToCategory;

class EventController extends Controller
{
    public function init() {
        if(Yii::$app->user->isGuest) {
            return $this->goHome();
        }
    }

    public function actionAdd() {
        if(isset($_GET['id']) && !empty($_GET['id'])) {
            $model = Event::find()->where(['id' => $_GET['id'], 'user_id' => Yii::$app->user->id])->one();
            foreach(RelEventToCategory::find()->where(['event_id' => $model->id])->all() as $c) 
                $model->category_ids[] = $c->event_category_id;
            if(!$model)
                return $this->goHome();
        } else {
            $model = new Event;
        }
        $category_arr = ArrayHelper::map(EventCategory::getCategoryList(), 'id', 'name');
        $city_arr = ArrayHelper::map(City::find()->orderBy('name ASC')->asArray()->all(), 'id', 'name');
        if($model->isNewRecord)
            $this->view->title = 'Новое мероприятие — YOweekend.ru';
        else 
            $this->view->title = 'Редактирование мероприятия — YOweekend.ru';
        
        $model->user_id = Yii::$app->user->id;
        if(isset($_POST['Event']) && !empty($_POST['Event'])) {
            if($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->image_original = str_replace("\\", "/", $model->image_original);
                $model->image_preview = str_replace("\\", "/", $model->image_preview);
                //Копируем основное изображение из темповой папки в основную
                $directory = $_SERVER['DOCUMENT_ROOT'].'/media/userimages' . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR;
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                }
		$ext_arr = explode('.', $model->image_original);
                $ext = array_pop($ext_arr);
                $_origin_file_name = uniqid(time(), true) . '.' . $ext;
                copy($_SERVER['DOCUMENT_ROOT'] . $model->image_original, $directory . $_origin_file_name);
                $model->image_original = '/media/userimages' . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR . $_origin_file_name;
                //Создаем превью размером 300x300
                $imagine = new Image;
                $imagine::getImagine()->open($_SERVER['DOCUMENT_ROOT'] . $model->image_preview)
                    ->crop(new Point($model->x1, $model->y1), new Box(300 , 300))
                    ->save($directory . 'preview_' . $_origin_file_name);
                $model->image_preview = '/media/userimages' . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR . 'preview_' . $_origin_file_name;                
                if($model->isNewRecord)
                    $model->create_date = date('Y-m-d H:i:s');
                else
                    $model->update_date = date('Y-m-d H:i:s');
                if($model->save()) {
                    RelEventToCategory::deleteAll(['event_id' => $model->id]);
                    foreach($model->category_ids as $id) {
                        $relation = new RelEventToCategory;
                        $relation->event_id = $model->id;
                        $relation->event_category_id = $id;
                        $relation->save();
                        unset($relation);
                    }
                    Yii::$app->session->setFlash('success', 'Ваше мероприятия успешно сохранено, теперь его видят пользователи в ленте.');
                    return $this->redirect('/event/edit/'.$model->id);
                } else {
                    Yii::$app->session->setFlash('error', 'В процессе сохранения произошла ошибка.');
                }
                
            } else {
                Yii::$app->session->setFlash('error', 'В процессе сохранения произошла ошибка.');
            }
        }
        return $this->render('add',[
            'model' => $model,
            'category_arr' => $category_arr,
            'city_arr' => $city_arr,
        ]);
    }
    
    public function actionDelete() {
        if(!Yii::$app->request->isAjax) {
            echo json_encode(['status' => false, 'error' => 'Это не Аякс запрос.']);   
            Yii::$app->end();
        }
        
        $event_id = Yii::$app->request->post('id', 0);        
        $event = Event::find()->where(['id' => $event_id])->one();
        if(!$event) {
            echo json_encode(['status' => false, 'error' => 'По какой-то причине данного мероприятия не существует.']);   
            Yii::$app->end();
        }
        if($event->user_id != Yii::$app->user->id) {
            echo json_encode(['status' => false, 'error' => 'Данное мероприятие принадлежит не вам.']);   
            Yii::$app->end();
        }
        
        if($event->delete()) {
            echo json_encode(['status' => true]);   
            Yii::$app->end();
        } else {
            echo json_encode(['status' => false, 'error' => 'В процессе удаления произошла ошибка']);   
            Yii::$app->end();
        }
    }
}