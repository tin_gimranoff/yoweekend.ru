<?php
/**
 * Created by PhpStorm.
 * User: tin
 * Date: 26.11.2017
 * Time: 21:55
 */

namespace app\controllers;

use Yii;
use app\models\Event;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use Imagine\Image\Box;

class MediaController extends Controller
{
    public function actionUpload() {                       
        $model = new Event();

        $imageFile = UploadedFile::getInstance($model, 'image');
        if(!in_array($imageFile->getExtension(), ['jpg', 'png', 'jpeg'])) {
            return Json::encode([
                'status' => false,
                'textError' => 'Вы можете загружать только файлы изображений с разрешениями png, jpg, jpeg.'
            ]);
        }

        $directory = $_SERVER['DOCUMENT_ROOT'].'/media/temp' . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR;
        if (!is_dir($directory)) {
            FileHelper::createDirectory($directory);
        }

        if ($imageFile) {
            $uid = uniqid(time(), true);
            $fileName = $uid . '.' . $imageFile->extension;
            $filePath = $directory . $fileName;
            if ($imageFile->saveAs($filePath)) {
                $path = '/media/temp/' . Yii::$app->session->id . DIRECTORY_SEPARATOR . $fileName;                
                $previewObjSize = $model->getPreviewSize(getimagesize($_SERVER['DOCUMENT_ROOT'].$path));
                $file_preview_path = '/media/temp' . DIRECTORY_SEPARATOR . Yii::$app->session->id . DIRECTORY_SEPARATOR . 'preview_'. $fileName;
                $box = new Box($previewObjSize['width'], $previewObjSize['height']);
                $preview = Image::getImagine()->open($_SERVER['DOCUMENT_ROOT'].$path);
                $preview->resize($box)->save($_SERVER['DOCUMENT_ROOT'].$file_preview_path);
                return Json::encode([
                    'files' => [
                        [
                            'name' => $fileName,
                            'size' => $imageFile->size,
                            'url' => $path,
                            'thumbnailUrl' => $file_preview_path,
                            'deleteUrl' => 'image-delete?name=' . $fileName,
                            'deleteType' => 'POST',
                            'width' => $previewObjSize['width'],
                            'height' => $previewObjSize['height'],
                        ],
                    ],
                ]);
            }
        }
    }
}