<?php
/**
 * Created by PhpStorm.
 * User: tin
 * Date: 26.11.2017
 * Time: 21:55
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Event;
use app\models\ChangePasswordForm;

class AccountController extends Controller
{
    public function init() {
        if(Yii::$app->user->isGuest)
            return $this->goHome();
    }
    
    public function actionMyEvents() {
        $is_draft = Yii::$app->request->get('isDraft', NULL);
        $events = Event::find()->where(['user_id' => Yii::$app->user->id]);
        if($is_draft != NULL && $is_draft == 'drafts')
            $events = $events->andWhere(['isDraft' => 1]);
        $events = $events->orderBy('meeting_date, time_begin')
                         ->all();
        $this->view->title = 'Мои мероприятия — YOweekend.ru';
        return $this->render('myEvents',[
            'events' => $events, 
            'is_draft' => $is_draft,
        ]);
    }
    
    public function actionChangePassword() {
        if(Yii::$app->user->isGuest)
            return $this->goHome();
        $user = \app\models\User::find()->where(['id' => Yii::$app->user->id])->one();
        if(!$user)
            return $this->goHome();
        $model = new ChangePasswordForm($user);
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранен.');
        }
        $this->view->title = 'Смена пароля — YOweekend.ru';
        return $this->render('changePassword', [
            'model' => $model,
        ]);
    }
    
}