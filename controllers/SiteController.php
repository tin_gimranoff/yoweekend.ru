<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Event;
use app\models\City;
use app\models\Filter;
use app\components\AuthHandler;

//'event' => 'event/list', - Удалить этот роут
class SiteController extends Controller {

    public function actions() {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function actionIndex() {
        $cookies = Yii::$app->getRequest()->getCookies();
        //Если нет города          
        if (!isset($_GET['city']) || empty($_GET['city'])) {
            if ($cookies->has('city') === true) {
                return $this->redirect('/' . $cookies['city']->value);
            } else {
                Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
                    'name' => 'city',
                    'value' => 'obninsk',
                    'expire' => time() + 60 * 60 * 24 * 30,
                    'path' => '/',
                    'httpOnly' => false,
                ]));
                return $this->redirect('/obninsk');
            }
        } else {
            $city = \app\models\City::find()->where(['alias' => $_GET['city']])->one();
            if ($city) {
                if ($cookies->has('city') === true && $cookies['city']->value != $city->alias) {
                    Yii::$app->getResponse()->getCookies()->remove('city');
                    Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
                        'name' => 'city',
                        'value' => $city->alias,
                        'expire' => time() + 60 * 60 * 24 * 30,
                        'path' => '/',
                        'httpOnly' => false,
                    ]));
                }
            } else {
                Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
                    'name' => 'city',
                    'value' => 'obninsk',
                    'expire' => time() + 60 * 60 * 24 * 30,
                    'path' => '/',
                    'httpOnly' => false,
                ]));
                return $this->redirect('/' . $cookies['city']->value);
            }
        }
        //Обределяем режим работы мероприятия=1 или организации=2
        if ($cookies->has('mode') === false || !in_array($cookies['mode']->value, [1, 2])) {
            Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
                'name' => 'mode',
                'value' => '1',
                'expire' => time() + 60 * 60 * 24 * 30,
                'path' => '/',
                'httpOnly' => false,
            ]));
            return $this->refresh();
        }

        $city = City::find()->where(['alias' => $_GET['city']])->one();

        $events = Event::find()->joinWith('categoriesIds')
                ->where(['city_id' => $city->id])
                ->andWhere(['isDraft' => 0]);
        $category_id = Yii::$app->request->get('category', null);
        if (empty(Yii::$app->request->get('category'))) {
            $filter = new Filter;
            if (empty($filter->meeting_date) && (empty($filter->time_begin) || $filter->time_begin == '00:00')) {
                //$events = $events->andWhere(['>=', 'CONCAT(meeting_date, " ",time_begin)', date('Y-m-d H:i:s')]);
		$events = $events->andWhere(['OR', ['>=', 'CONCAT(meeting_date, " ",time_begin)', date('Y-m-d H:i:s')], ['AND', ['>=','meeting_date',date('Y-m-d')], 'no_time' => 1]]);
            } elseif (empty($filter->meeting_date) && !empty($filter->time_begin) && $filter->time_begin != '00:00') {
                $events = $events->andWhere(['>=', 'meeting_date', date('Y-m-d')]);
                $events = $events->andWhere(['>=', 'time_begin', $filter->time_begin]);
            } elseif (!empty($filter->meeting_date)) {
                $events = $events->andWhere(['>=', 'meeting_date', $filter->meeting_date]);
                if (!empty($filter->time_begin))
                    $events = $events->andWhere(['>=', 'time_begin', $filter->time_begin]);
            }
            if (!empty($filter->category))
                $events = $events->andWhere(['rel_event_to_category.event_category_id' => $filter->category]);
        } else {
            $category_id = \app\models\EventCategory::find()->where(['alias' => Yii::$app->request->get('category')])->one()->id;
            $filter = new Filter;
            $filter = $filter->set_filter_by_category_link($category_id);
            $events = $events->andWhere(['>=', 'CONCAT(meeting_date, " ",time_begin)', date('Y-m-d H:i:s')]);
            $events = $events->andWhere([
                'rel_event_to_category.event_category_id' => $category_id
            ]);
        }
        $events = $events->orderBy('meeting_date, time_begin')
                ->limit(Yii::$app->params['limit_objects_on_page'])
                ->groupBy('id')
                ->all();
        //Если в урле у нас передается ID события начинаем вытаскивать по нему инфомацию из БД
        $event_id = Yii::$app->request->get('event_id', null);
        $event_info = null;
        $event_info_text = null;
        if ($event_id != null) {
            if (!Event::find()->where(['id' => $event_id])->exists()) {
                $event_info = null;
            } else {
                $event_info = Event::find()->where(['id' => $event_id])->one();
                $event_info_text = preg_replace('/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[.\!\/\\w]*))?)/i', '<a href="$1" target="_blank">$1</a>', $event_info->description);
                $event_info_text = preg_replace('/<a href="([A-Za-z0-9_.]+@[A-Za-z0-9_.]+)"/i', '<a href="mailto:$1"', $event_info_text);
                $event_info_text = nl2br($event_info_text);
            }
        }
        //Определяем мета теги
        if ($category_id == null && $event_id == null) {
            $this->view->title = 'Афиша мероприятий вашего города — ' . $city->name . ' — YOweekend.ru';
            $this->view->registerMetaTag([
                'name' => 'description',
                'content' => 'Афиша предстоящих мероприятий в городе ' . $city->name
            ]);
            $this->view->registerMetaTag([
                'name' => 'keywords',
                'content' => 'афиша мероприятий, мероприятия, события, куда сходить, ' . $city->name,
            ]);
        } elseif ($category_id != NULL) {
            $category = \app\models\EventCategory::find()->where(['id' => $category_id])->one();
            if ($category) {
                $this->view->title = $category->name . ' — Афиша мероприятий вашего города —' . $city->name . ' — YOweekend.ru';
                $this->view->registerMetaTag([
                    'name' => 'description',
                    'content' => 'Афиша предстоящих мероприятий в городе ' . $city->name . ' в категории ' . $category->name,
                ]);
                $this->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => 'афиша мероприятий, мероприятия, события, куда сходить, ' . $city->name . ', ' . $category->name,
                ]);
            }
        } elseif ($event_id != NULL) {
            if ($event_info) {
                $this->view->title = $event_info->name . ' — Афиша мероприятий вашего города — ' . $city->name . ' — YOweekend.ru';
                $this->view->registerMetaTag([
                    'name' => 'description',
                    'content' => mb_strimwidth($event_info->description, 0, 100, ''),
                ]);
                $this->view->registerMetaTag([
                    'name' => 'keywords',
                    'content' => $event_info->name,
                ]);
            }
        }

        if ($event_info != NULL)
            $this->view->title = $event_info->name . ' — ' . $this->view->title;

        return $this->render('index', [
                    'events' => $events,
                    'city' => $city,
                    'filter_query' => json_encode($filter->attributes),
                    'filter' => $filter,
                    'event_info' => $event_info,
                    'event_info_text' => $event_info_text,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $this->view->title = 'Авторизация — YOweekend.ru';
        $this->layout = 'main_mini';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRegister() {
        $this->view->title = 'Регистрация — YOweekend.ru';
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        $this->layout = 'main_mini';
        return $this->render('register', [
                    'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $this->view->title = 'Сброс пароля — YOweekend.ru';

        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте свою почту там дальнейшие инструкции.');
                return $this->redirect('/login');
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем использовать данный Email адрес.');
            }
        }

        $this->layout = 'main_mini';
        return $this->render('passwordResetRequestForm', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        $this->view->title = 'Сброс пароля — YOweekend.ru';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранен.');
            return $this->redirect('/login');
        }

        $this->layout = 'main_mini';
        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function onAuthSuccess($client) {
        (new AuthHandler($client))->handle();
    }

    public function actionSitemap() {
        header('Content-Type: text/xml');
        $urls[] = [
            'loc' => 'http://' . $_SERVER['SERVER_NAME'] . '/obninsk',
            'changefreq' => 'hourly',
            'priority' => 0.8,
        ];
        $events_category = \app\models\EventCategory::find()->all();
        foreach ($events_category as $c) {
            $urls[] = [
                'loc' => 'http://' . $_SERVER['SERVER_NAME'] . '/obninsk/' . $c->alias,
                'changefreq' => 'hourly',
                'priority' => 0.8,
            ];
        }
        $events = Event::find()->where(['isdraft' => 0])->all();
        foreach ($events_category as $e) {
            $urls[] = [
                'loc' => 'http://' . $_SERVER['SERVER_NAME'] . '/obninsk/event_' . $e->id,
                'priority' => 0.8,
            ];
        }
        
        echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
            echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            foreach ($urls as $url){
                echo '<url>';
                foreach($url as $key=>$value) {
                   echo '<'.$key.'>'.$value.'</'.$key.'>';                  
                }
                echo '</url>';
            }
            echo '</urlset>';   
        Yii::$app->end(); 
    }

}
