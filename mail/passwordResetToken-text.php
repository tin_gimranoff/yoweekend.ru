<?php
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

    Приветствуем вас, <?= $user->username ?>,
    Перейдите по ссылке для сброса пароля:

<?= $resetLink ?>