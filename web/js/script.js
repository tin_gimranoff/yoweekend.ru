$(document).ready(function () {
    var isMobile = {
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            }
    }
    //Подстройка адреса под контент и ширину карточки
    thumbnail_address_resize();
    //Настройка дэйтпикера
    $.fn.datepicker.dates['ru'] = {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        daysShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня",
        clear: "Сбросить",
        weekStart: 1,
    };
    //Поле для дэйтпикера
    $(".datepicker-modal").datepicker({
        language: 'ru',
        clearBtn: true,
        todayBtn: "linked",
        zIndexOffset: 1050,
        format: 'yyyy-mm-dd',
        autoclose:true,
    }).on('hide', function(e) {
        $(this).blur();
    });
    $(".timepicker").timepicker({
        showInputs: false,
        showMeridian: false,
        defaultTime: '00:00'
    });
    //Функция ресайза для окна
    $(window).bind('resize', function () {
        thumbnail_address_resize();
        //СЛИМ СКРОЛЛ
        if(!isMobile.iOS()) {
            $(".sidebar-category-links").slimScroll({
                color: '#FFFFFF',
                height: $(window).height() - $(".main-header").height(),
            });
        }
    });
    //Контекстное меню
    var actions = [{
            name: 'Открыть афишу',
            onClick: function ($item) {
                Shadowbox.open({
                    content: $item.data('fullImg'),
                    player: 'img',
                });
            }
        }, {
            name: 'Подробная информация',
            onClick: function ($item) {
                showFullInfo($item.data('id'), '/ajax/get-full-info-modal');
            }
        }, {
            name: 'Открыть карту',
            onClick: function ($item) {
                openMap($item.data('address'));
            }
        }];
    if (location.href.indexOf('account/my-events') + 1) {
        actions.push({
            name: 'Редактировать',
            onClick: function ($item) {
                window.location.href = '/event/edit/' + $item.data('id');
            }
        }, {
            name: 'Удалить',
            onClick: function ($item) {
                var result = confirm("Вы уверены что хотите удалить мероприятие?");
                if (result == true) {
                    $.ajax({
                        url: '/event/delete',
                        type: 'post',
                        data: {id: $item.data('id')},
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == true) {
                                $item.parent().parent().parent().remove();
                            } else {
                                show_info_message('Произошла ошибка', data.error);
                            }
                        },
                        error: function () {
                            show_info_message('Произошла ошибка', 'неизвестная ошибка');
                        }
                    });
                }
            }
        });
    }
    var menu = new BootstrapMenu('.thumbnail img', {
        menuEvent: 'click',
        fetchElementData: function ($item) {
            return $item;
        },
        actions: actions,
    });

    //Инициируем шадоубокс
    Shadowbox.init();
    $(window).scroll(function () {
        //Скрываем контекстные меню
        $(".dropdown.bootstrapMenu").hide();
        //Кнопка вверх
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
        //Даты уводим в шапку при скролле
        var El = $(".content-wrapper .date-label-area");

        $.each(El, function (index, i) {
            size = $(i).offset().top - $(i).height();
            if ($(window).scrollTop() > size) {
                $(".main-header .date-label").empty().append($(El[index]).text());
            }
        });
    });
    //Кнопка вверх
    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    //Initialize Select2 Elements
    $('.select2').select2();
    //Обрезка изображения
    $('.img_preview img').imgAreaSelect({
        aspectRatio: '1:1',
        handles: true,
        onSelectEnd: setImgCoordinates,
        //onInit: setImgCoordinates,
        minWidth: 299,
        minHeight: 299,
        maxWidth: 300,
        maxHeight: 300,
        show: true,
        imageHeight: $(".img_preview img").attr('data-real-height'),
        imageWidth: $(".img_preview img").attr('data-real-width'),
        x1: $("#event-x1").val(),
        x2: $("#event-x2").val(),
        y1: $("#event-y1").val(),
        y2: $("#event-y2").val(),
    });

    //Дробдаун отвечающий за режим работы
    $(".mode-dropdown li a").bind('click', function () {
        $.cookie('mode', $(this).attr('data-value'), {path: '/'});
        window.location.reload();
    });
    //СЛИМ СКРОЛЛ
    if(!isMobile.iOS()) {
        $(".sidebar-category-links").slimScroll({
            color: '#FFFFFF',
            height: $(window).height() - $(".main-header").height(),
        });
    }
    //Черновики в личном кабинете
    $('.isDraftLink input').on('ifChanged', function (event) {
        if ($(this).is(':checked'))
            window.location.href = "/account/my-events/drafts";
        else
            window.location.href = "/account/my-events";
    });

    //Предложение на добавление новой категории
    $("#send-new-category").bind('click', function () {
        var new_category = $("#add-new-category input").val();
        if (new_category == '') {
            $("#add-new-category .category-name").addClass('has-error');
            $("#add-new-category p").empty().append('Название не может быть пустым.');
            return;
        }
        $("#add-new-category .category-name").removeClass('has-error');
        $("#add-new-category p").empty();
        $.ajax({
            url: '/ajax/send-new-category',
            type: 'post',
            data: {category_name: new_category},
            dataType: 'json',
            success: function (data) {
                if(data.status == true) {
                    $("#add-new-category .callout").addClass('callout-success');
                } else {
                    $("#add-new-category .callout").addClass('callout-warning');
                }
                $("#add-new-category .callout span").empty().append(data.textMessage);
                $("#add-new-category .callout").show();
            },
        });
    });
    
    //При скрытии формы добавления категории очищаем ее
    $('#add-new-category').on('hidden.bs.modal', function (e) {
        $("#add-new-category .callout").removeClass('callout-warning');
        $("#add-new-category .callout").removeClass('callout-success');
        $("#add-new-category .callout span").empty();
        $("#add-new-category .callout").hide();
        $("#add-new-category input").val('');
    });
    
    //Запрещаем прокручивать контент если открыт фильтр
    $(document).on('expanded.pushMenu', function(){
           $("html,body").css("overflow", "hidden");
    });
    $(document).on('collapsed.pushMenu', function(){
           $("html,body").css({"overflow-x":"hidden", "overflow-y":"auto"});
    });
    //Запрещаем двигать контент, когда открыт фильтр
    $('#modal-filter').on('shown.bs.modal', function (e) {
        $("html,body").css("overflow", "hidden");
    });
    $('#modal-filter').on('hidden.bs.modal', function (e) {
        $("html,body").css({"overflow-x":"hidden", "overflow-y":"auto"});
    });

    $(document).on('copy', '.intro, #info-modal .modal-body', function(event){         
        var clipboardData = (event.clipboardData || window.event.clipboardData);
        if (clipboardData) {
            clipboardData.setData('text/plain', 'Копирование с сайта не одобряется администраторами!');
            event.preventDefault();
        } else {
            alert('Clipboard API не поддерживается!');
        }
    });
    
});

function openMap(address) {
    if ($(document).width() < 800) {
        var mapWidth = $(document).width() - 50;
        var modalWidth = $(document).width() - 20;
    } else {
        var mapWidth = 770;
        var modalWidth = 800;
    }

    if ($(window).height() < 600) {
        var mapHeight = $(window).height() - 115;
    } else {
        var mapHeight = 500;
    }

    $("#info-modal .modal-header").hide();
    $("#info-modal .modal-dialog").css('width', modalWidth);
    $("#info-modal .modal-body").empty().append('<div id="map" style="width: ' + mapWidth + 'px; height: ' + mapHeight + 'px;"></div>');
    ymaps.ready(init(address));
    var myMap;
    $("#info-modal").modal();
}

function showFullInfo($objId, url) {
    $.ajax({
        url: url,
        type: 'post',
        data: {id: $objId},
        dataType: 'json',
        success: function (data) {
            show_info_modal(data.name, data.html);
            load_layout('none');
        },
        beforeSend: function () {
            load_layout('block');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            load_layout('none');
            show_info_modal('Ошибка', 'Сожалеем, но в процессе загрузки произошла ошибка. Повторите запрос еще раз.');
            console.log(jqXHR, textStatus, errorThrown);
        }
    });
}

function thumbnail_address_resize() {
    var im_width = $(".thumbnail img").width();
    var address_width = $(".thumbnail .address").width();
    if (address_width > im_width) {
        $(".thumbnail .address").css({'width': im_width});
    } else {
        $(".thumbnail .address").css({'width': 'auto'});
    }
}

function show_info_modal(title, content) {
    if (title == '')
        $("#info-modal .modal-header").hide();
    else {
        $("#info-modal .modal-header").show();
        $("#info-modal .modal-title").empty().append(title);
    }

    $("#info-modal .modal-body").empty().append(content);

    $("#info-modal").modal();
}

function init(address) {
    myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 14
    });
    ymaps.geocode(address, {
        /**
         * Опции запроса
         * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
         */
        // Сортировка результатов от центра окна карты.
        // boundedBy: myMap.getBounds(),
        // strictBounds: true,
        // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
        // Если нужен только один результат, экономим трафик пользователей.
        results: 1
    }).then(function (res) {
        // Выбираем первый результат геокодирования.
        var firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                coords = firstGeoObject.geometry.getCoordinates();
        // Область видимости геообъекта.
        //bounds = firstGeoObject.properties.get('boundedBy');

        firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
        // Получаем строку с адресом и выводим в иконке геообъекта.
        firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());
        /*myMap.setBounds(bounds, {
         // Проверяем наличие тайлов на данном масштабе.
         checkZoomRange: true
         });*/
        // Добавляем первый найденный геообъект на карту.
        myMap.geoObjects.add(firstGeoObject);
        // Масштабируем карту на область видимости геообъекта.                      
        var myPlacemark = new ymaps.Placemark(coords, {
            iconContent: '',
            //balloonContent: 'Нижний Новгород'
        }, {
            preset: 'islands#violetStretchyIcon'
        });
        myMap.geoObjects.add(myPlacemark);
        myMap.setCenter(coords);
        myMap.setZoom(16);
        //myMap.panTo(coords);                    
    });
}

function setImgCoordinates(img, selection) {
    $("#event-x1").val(selection.x1);
    $("#event-x2").val(selection.x2);
    $('#event-y1').val(selection.y1);
    $("#event-y2").val(selection.y2);
    image_width = selection.width;
    image_height = selection.height;
}

function load_layout(status) {
    $(".custom-load-layout").css('display', status);
    if(status == 'block')
        $("html,body").css("overflow", "hidden");
    if(status == 'none')
        $("html,body").css({"overflow-x":"hidden", "overflow-y":"auto"});
}