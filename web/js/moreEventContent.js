$(document).ready(function () {
    /* Переменная-флаг для отслеживания того, происходит ли в данный момент ajax-запрос. В самом начале даем ей значение false, т.е. запрос не в процессе выполнения */
    var inProgress = false;
    /* С какой статьи надо делать выборку из базы при ajax-запросе */
    var startFrom = 25;
    var filter_data = $("input[name=filter_data]").val();
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !inProgress) {
            var date = $(".content-wrapper .date-label");
            date = $(date[date.length - 1]).text();
            $.ajax({
                url: '/ajax/get-event-content',
                method: 'POST',
                data: {"startFrom": startFrom, "lastDate": date, "filterData": filter_data},
                beforeSend: function () {
                    /* меняем значение флага на true, т.е. запрос сейчас в процессе выполнения */
                    inProgress = true;
                }
                /* что нужно сделать по факту выполнения запроса */
            }).done(function (data) {
                /* Если массив не пуст (т.е. статьи там есть) */
                if (data.length > 0) {
                    /* По факту окончания запроса снова меняем значение флага на false */
                    inProgress = false;
                    // Увеличиваем на 5 порядковый номер статьи, с которой надо начинать выборку из базы
                    startFrom += 25;
                    $(".content-container").append(data);
                }
            });
        }
    });

    $(".more-load-btn button").bind('click', function () {
        if (!inProgress) {
            var date = $(".content-wrapper .date-label");
            date = $(date[date.length - 1]).text();
            $.ajax({
                url: '/ajax/get-event-content',
                method: 'POST',
                data: {"startFrom": startFrom, "lastDate": date, "filterData": filter_data},
                beforeSend: function () {
                    /* меняем значение флага на true, т.е. запрос сейчас в процессе выполнения */
                    inProgress = true;
                }
                /* что нужно сделать по факту выполнения запроса */
            }).done(function (data) {
                /* Если массив не пуст (т.е. статьи там есть) */
                if (data.length > 0) {
                    /* По факту окончания запроса снова меняем значение флага на false */
                    inProgress = false;
                    // Увеличиваем на 5 порядковый номер статьи, с которой надо начинать выборку из базы
                    startFrom += 25;
                    $(".content-container").append(data);
                }
            });
        }
    });
});

