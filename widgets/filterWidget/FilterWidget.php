<?php
namespace app\widgets\FilterWidget;


use yii\web\View;
use Yii;
use app\models\EventCategory;
use app\models\Filter;
use yii\helpers\ArrayHelper;

class FilterWidget extends \yii\base\Widget {
    
    public $view = 'form';

    public function init() {
        
    }
    
    public function run() {
        $filter = new Filter;
        $category_arr = ArrayHelper::map(EventCategory::getCategoryList(), 'id', 'name');
        return $this->render($this->view, [
            'filter' => $filter,
            'category_arr' => $category_arr,
        ]);
    }
}

    