<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="modal fade in" id="modal-filter">
    <div class="modal-dialog">
<?php $form = ActiveForm::begin(
        [
            'action' => '/'.\app\models\City::detectSavedCity(true)->alias,
            'options' => [
                'class' => 'filter-form'
             ]
        ]
) ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Настройки фильтра</h4>
            </div>
            <div class="modal-body">
                    <?=
                    $form->field($filter, 'category', [
                        'template' => '{label}{input}{error}{hint}'
                    ])->dropDownList($category_arr, ['class' => 'form-control select2', 'multiple' => true, 'aria-hidden' => true])
                    ?>
                    <?=
                    $form->field($filter, 'meeting_date', [
                        'template' => '{label}<div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div>{input}</div>{error}{hint}'
                    ])->textInput(['class' => 'form-control datepicker-modal', 'data-inputmask' => "'alias': 'dd/mm/yyyy'", 'data-mask' => "", "readonly" => true])
                    ?>
                    <div class="bootstrap-timepicker">
                        <?=
                        $form->field($filter, 'time_begin', [
                            'template' => '{label}<div class="input-group"><div class="input-group-addon"><i class="fa fa-clock-o"></i></div>{input}</div>{error}{hint}'
                        ])->textInput(['class' => 'form-control input-small timepicker', "readonly" => true])
                        ?>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                <button type="submit" class="btn btn-primary pull-left">Показать</button>
            </div>
        </div>
<?php ActiveForm::end(); ?>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>