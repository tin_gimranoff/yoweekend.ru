<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin(
        [
            'action' => '/'.\app\models\City::detectSavedCity(true)->alias,
            'options' => [
                'class' => 'sidebar-filter filter-form'
             ]
        ]
) ?>
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Категория</li>
        <!-- Optionally, you can add icons to the links -->
        <li>
            <?=
            $form->field($filter, 'category', [
                'template' => '{input}'
            ])->dropDownList($category_arr, ['class' => 'form-control select2', 'multiple' => true, 'aria-hidden' => true])->label(false);
            ?>
        </li>
        <li class="header">Дата</li>
        <!-- Optionally, you can add icons to the links -->
        <li>
            <?=
            $form->field($filter, 'meeting_date', [
                'template' => '<div class="input-group"><div class="input-group-addon"><i class="fa fa-calendar"></i></div>{input}</div>'
            ])->textInput(['class' => 'form-control datepicker-modal', 'data-inputmask' => "'alias': 'dd/mm/yyyy'", 'data-mask' => "", "readonly" => true])->label(false);
            ?>
        </li>
        <li class="header">Время</li>
        <li>
            <div class="bootstrap-timepicker">
                <?=
                $form->field($filter, 'time_begin', [
                    'template' => '<div class="input-group"><div class="input-group-addon"><i class="fa fa-clock-o"></i></div>{input}</div>'
                ])->textInput(['class' => 'form-control input-small timepicker', "readonly" => true])->label(false);
                ?>
            </div>
        </li>
        <li>
            <button class="btn btn-block btn-sm" style="width: 50%;margin-top:22px; float: right">Показать</button>
        </li>
    </ul>
<?php ActiveForm::end(); ?>
