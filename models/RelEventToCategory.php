<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rel_event_to_category".
 *
 * @property integer $id
 * @property integer $event_id
 * @property integer $event_category_id
 */
class RelEventToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rel_event_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'event_category_id'], 'required'],
            [['event_id', 'event_category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'event_category_id' => 'Event Category ID',
        ];
    }
}
