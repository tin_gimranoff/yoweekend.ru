<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Filter extends Model {

    public $category;
    public $meeting_date;
    public $time_begin;

    public function init() {
        $cookies = Yii::$app->getRequest()->getCookies();
        if (Yii::$app->request->post('Filter')) {
            $this->attributes = Yii::$app->request->post('Filter');
            $this->setFilterCookie('category', json_encode($this->category));
            $this->setFilterCookie('meeting_date', $this->meeting_date);
            $this->setFilterCookie('time_begin', $this->time_begin);
        } else {
            if($cookies->has('category'))
                $this->category = json_decode($cookies['category']->value);
            if($cookies->has('meeting_date'))
                $this->meeting_date = $cookies['meeting_date']->value;
            if($cookies->has('time_begin'))
                $this->time_begin = $cookies['time_begin']->value;
        }
    }
    
    public function rules() {
        return [
            [['category', 'meeting_date', 'time_begin'], 'safe']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'category' => 'Категория',
            'meeting_date' => 'Начиная с',
            'time_begin' => 'Время начало с',
        ];
    }
    
    public function set_filter_by_category_link($category_id) {
        $this->meeting_date = '';
        $this->time_begin = '00:00';
        $this->category = array($category_id);
        return $this;
    }
    
    public function getStringCategories() {
        if(empty($this->category))
            return ['Все категории'];
        $categories = EventCategory::find()->where(['id' => $this->category])->orderBy('position, name ASC')->all();
        $categories = \yii\helpers\ArrayHelper::map($categories, 'id', 'name');
        return $categories;
    }

    private function setFilterCookie($name, $value) {        
        Yii::$app->getResponse()->getCookies()->add(new \yii\web\Cookie([
            'name' => $name,
            'value' => $value,
            'expire' => time() + 60 * 60 * 24 * 30,
            'path' => '/',
            'httpOnly' => false,
        ]));
    }

}
