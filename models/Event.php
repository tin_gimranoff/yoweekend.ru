<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $name
 * @property string $image_original
 * @property string $image_preview
 * @property string $link
 * @property integer $city_id
 * @property string $address
 * @property string $meeting_date
 * @property string $time_begin
 * @property integer $no_time
 * @property integer $price
 * @property string $description
 * @property integer $isDraft
 * @property integer $isVkSend
 */
class Event extends \yii\db\ActiveRecord
{
    
    public $image;
    public $category_ids;
    public $x1, $x2, $y1, $y2;
    /**
     * @inheritdoc
     */
    
    public function init() {
        $this->image_original = '/img/temp_image.png';
        $this->image_preview = '/img/temp_image.png';
        $this->x1 = 0;
        $this->x2 = 299;
        $this->y1 = 0;
        $this->y2 = 299;
    }


    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city_id', 'meeting_date', 'time_begin', 'category_ids', 'user_id', 'address'], 'required'],
            [['meeting_date'], 'date'],
            [['time_begin'], 'time'],
            [['city_id', 'price', 'isDraft', 'isVkSend', 'x1', 'x2', 'y1', 'y2', 'user_id', 'no_time'], 'integer'],
            [['meeting_date', 'time_begin', 'no_time'], 'safe'],
            [['description'], 'string'],
            [['name', 'image_original', 'image_preview', 'link', 'address'], 'string', 'max' => 255],
            [['link'], 'url'],
            ['x1', function ($attribute, $params) {
                if(abs($this->x1-$this->x2) > 300 || abs($this->y1-$this->y2) > 300)
                    $this->addError($attribute, 'Выбранная область изображения больше чем разрешено. Выделите правильную область.');
            }],
            ['image_original', function($attribute, $params){
                if(!file_exists($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->image_original) || !file_exists($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->image_preview)) 
                    $this->addError($attribute, 'При преобразовании изображения возникла ошибка.');
            }],
            ['category_ids', function($attribute, $params) {
                if(count($this->category_ids) > 3) 
                    $this->addError($attribute, 'Вы можете выбрать только 3 категории.');
            }],
            [['create_date', 'update_date'], 'safe'],
        ];
    }
    
    public function getCategoriesIds()
    {
        return $this->hasMany(RelEventToCategory::className(), ['event_id' => 'id']);
    }
    
    public function getCity() {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название мероприятия',
            'image_original' => 'Оригинал изображения',
            'image_preview' => 'Превью изображения',
            'link' => 'Ссылка',
            'city_id' => 'Город',
            'address' => 'Адрес',
            'meeting_date' => 'Дата мероприятия',
            'time_begin' => 'Время начала мероприятия',
            'no_time' => 'Не показывать время на сайте (не желательно)',
            'price' => 'Стоимость',
            'description' => 'Описание (форматирование сохраняется, ссылки будут активны)',
            'isDraft' => 'Черновик',
            'isVkSend' => 'Статус экспорта в VK',
            'category_ids' => 'Категории (не боллее 3-х)',
        ];
    }
    
    public function getPreviewSize($imagesize) {        
        $ratio = $imagesize[0]/$imagesize[1];
        $otions = [];
        if ($imagesize[0] > $imagesize[1]) {
            $options['height'] = 300;
            $options['width'] = round(300*$ratio);
        }
        elseif ($imagesize[1] > $imagesize[0]) {
            $options['width'] = 300;
            $options['height'] = round(300/$ratio);
        } else {
            $options['width'] = 300;
            $options['height'] = 300;
        }

        return $options;
    }
    
    public function getRealImgSize($filename) {
        $size = getimagesize($_SERVER['DOCUMENT_ROOT'] . str_replace('\\', '/', $filename));
        return ['width' => $size[0], 'height' => $size[1]];
    }
    
}
