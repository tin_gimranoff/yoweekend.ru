<?php

namespace app\models;

use yii\base\Model;
use app\models\User;

/**
 * Password reset form
 */
class ChangePasswordForm extends Model
{

    public $password;
    public $retype_password;

    /**
     * @var \app\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($user, $config = [])
    {

        $this->_user = $user;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'retype_password'], 'required'],
            ['password', 'string', 'min' => 6],
            ['retype_password', 'compare', 'compareAttribute' => 'password', 'operator' => '==', 'message' => 'Пароли должны совпадать'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'retype_password' => 'Повтор пароля',
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        return $user->save(false);
    }

}