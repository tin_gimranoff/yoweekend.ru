<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $hidden
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['hidden'], 'integer'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Алиас',
            'hidden' => 'Скрытый',
        ];
    }
    
    public function getCitiesArr() {
        return self::find()->where(['hidden' => 0])->orderBy('name')->asArray()->all();
    }
    
    public function getCityNameByAlias($alias) {
        return self::find()->where(['alias' => $alias])->one()->name;
    }
    
    public function detectSavedCity($fullInfo = false) {
        $cookie = Yii::$app->getRequest()->getCookies();
        if(!isset($_GET['city']) || empty($_GET['city'])) {
            if($cookie->has('city')) {
                if($fullInfo == false)
                    return self::getCityNameByAlias($cookie['city']->value);
                else
                    return self::find()->where(['alias' => $cookie['city']->value])->one();
            } else {
                if($fullInfo == false)
                    return self::getCityNameByAlias('obninsk');
                else
                    return self::find()->where(['alias' => 'obninsk'])->one();
            }
        } else {
            if($fullInfo == false)
                return self::getCityNameByAlias($_GET['city']);
            else
                return self::find()->where(['alias' => $_GET['city']])->one();
        }
    }
    
}
