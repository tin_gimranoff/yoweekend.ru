<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_categories".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $position
 */
class EventCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias'], 'required'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['name'], 'unique'],
            ['position', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Ссылка',
            'position' => 'Позиция в списках',
        ];
    }
    
    public function getCategoryList() {
        return self::find()->orderBy('position, name ASC')->all();
    }
    
}
